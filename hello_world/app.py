import os
import json
import pandas
import requests

URI = os.getenv('URI', 'https://jsonplaceholder.typicode.com/todos')


def lambda_handler(event, context):
    """Sample pure Lambda function

    Parameters
    ----------
    event: dict, required
        API Gateway Lambda Proxy Input Format

        Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format

    context: object, required
        Lambda Context runtime methods and attributes

        Context doc: https://docs.aws.amazon.com/lambda/latest/dg/python-context-object.html

    Returns
    ------
    API Gateway Lambda Proxy Output Format: dict

        Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
    """

    # try:
    #     ip = requests.get("http://checkip.amazonaws.com/")
    # except requests.RequestException as e:
    #     # Send some context about this error to Lambda Logs
    #     print(e)

    #     raise e

    resp = requests.get(URI).json()
    frame = pandas.DataFrame(resp)
    counts = frame.groupby('completed')['id'].count()

    return {
        "statusCode": 200,
        "body": json.dumps({
            "count": counts.to_dict(),
            "message": "hello world",
            # "location": ip.text.replace("\n", "")
        }),
    }
